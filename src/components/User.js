import React from 'react';
import { graphql, createFragmentContainer } from 'react-relay';

const User = (props) => {
    const {viewer: {id, name, login}} = props;
    return (
        <div>
            <div>User id: {id}</div>
            <div>User name: {name}</div>
            <div>User login: {login}</div>
        </div>
    )
};

export default createFragmentContainer(
    User, 
    graphql`
        fragment User_viewer on User {
            id
            name
            login
        }
    `
)