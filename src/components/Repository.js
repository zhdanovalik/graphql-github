import React from 'react';
import { graphql, createFragmentContainer } from 'react-relay';

const Repository = ({repository: {id, name, description}}) => {
    return (
        <div>
            <h3>Repository</h3>
            <ul>
                <li><strong>id:</strong>{id}</li>
                <li><strong>name:</strong>{name}</li>
                <li><strong>description:</strong>{description}</li>
            </ul>
        </div>
    )
};

export default createFragmentContainer(Repository, graphql`
    fragment Repository_repository on Repository {
        id
        name
        description
    }
`)