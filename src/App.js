import React, { Component } from 'react';
import {graphql, QueryRenderer} from 'react-relay';
import environment from './environment';

import User from './components/User';
import Repository from './components/Repository';

const query = graphql`
    query AppQuery($owner: String!, $name: String!) {
        viewer {
            ...User_viewer
        }
        repository (owner: $owner, name: $name) {
            ...Repository_repository
        }
    }
`;

export default class App extends Component {
    render() {
        return (
            <QueryRenderer
                environment={environment}
                query={query}
                variables={{owner: 'reactjs', name: 'redux'}}
                render={({error, props}) => {
                    if (props) {
                        return <div>
                            <User viewer={props.viewer} />
                            <Repository repository={props.repository} />
                        </div>;
                    } else  {
                        return <div>Loading...</div>;
                    }
                }}
            />
        );
    }
}
